/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   make_stacks2.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: amajid <amajid@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/01/03 19:47:49 by amajid            #+#    #+#             */
/*   Updated: 2024/01/03 19:50:45 by amajid           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"
#include <limits.h>
#include <stdlib.h>

char	is_err_work(t_err_v *v)
{
	while (v->ss[v->j][v->i])
	{
		if ((v->ss[v->j][v->i] < '0' || v->ss[v->j][v->i] > '9')
			&& (v->ss[v->j][v->i] != '+' && v->ss[v->j][v->i] != '-'))
			return (-1);
		if (v->is_mult_sign && (v->ss[v->j][v->i] == '-'
			|| v->ss[v->j][v->i] == '+'))
			return (-1);
		if (v->ss[v->j][v->i] == '-' || v->ss[v->j][v->i] == '+')
			v->is_mult_sign = 1;
		v->i++;
	}
	return (1);
}

char	is_err(char *s)
{
	t_err_v	v;

	v.is_n_fount = 0;
	v.j = 0;
	v.ss = ft_split(s, ' ');
	while (v.ss[v.j])
	{
		v.is_mult_sign = 0;
		v.num = ft_atoi(v.ss[v.j]);
		if (v.num > INT_MAX || v.num < INT_MIN)
			return (-1);
		v.i = 0;
		if (is_err_work(&v) == -1)
			return (-1);
		v.i--;
		if ((v.ss[v.j][v.i] < '0' || v.ss[v.j][v.i] > '9') || v.i > 10)
			return (-1);
		v.j++;
	}
	return (0);
}

char	check_for_eroors_0(int count, char **elements, int *a)
{
	int	i;

	i = 0;
	while (i < count)
	{
		if (is_err(elements[i]) == -1)
		{
			ft_putstr_fd("Error\n", 1);
			free(a);
			exit(0);
		}
		i++;
	}
	return (0);
}

t_stack	create_stack_b(int count)
{
	int		*stack;
	int		i;
	t_stack	result;

	stack = ft_calloc(count, sizeof(int));
	if (!stack)
		return ((t_stack){NULL, 0});
	i = 1;
	while (i < count)
	{
		stack[i] = 0;
		i++;
	}
	result = (t_stack){.arr = stack, 0};
	return (result);
}
