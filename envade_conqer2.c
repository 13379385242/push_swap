/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   envade_conqer2.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: amajid <amajid@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/01/03 17:57:19 by amajid            #+#    #+#             */
/*   Updated: 2024/01/03 17:58:01 by amajid           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int	get_min(int x, int y)
{
	if (x < y)
		return (x);
	else
		return (y);
}

void	push_but_3(t_stack *a, t_stack *b)
{
	if (a->size > 3)
	{
		while (a->size > 3)
		{
			pb(a, b);
		}
	}
}

long	get_cost_up_b(t_stack *a, t_stack *b, long long index, char *is_b_up)
{
	long	cost_to_up_push;
	char	is_neg;

	(void)a;
	is_neg = index > (b->size / 2);
	if (is_neg)
	{
		cost_to_up_push = b->size - index;
	}
	else
	{
		cost_to_up_push = index;
	}
	cost_to_up_push++;
	*is_b_up = is_neg;
	return (cost_to_up_push);
}

void	push_swap_max(t_stack *a, t_stack *b, long index)
{
	char	is_up;

	is_up = index <= (b->size / 2);
	while (index)
	{
		if (is_up)
		{
			rb(b);
			index--;
		}
		else
		{
			rrb(b);
			index++;
			if (index >= b->size)
				index = 0;
		}
	}
	pa(a, b);
	ra(a);
}

void	gcu_a_part0(t_gcu_a *v, t_vars vars)
{
	while (v->i < vars.a->size)
	{
		if (vars.a->arr[v->i] > v->target)
		{
			if (v->is_first)
			{
				v->min = vars.a->arr[v->i];
				v->min_index = v->i;
				v->is_first = 0;
			}
			v->min = v->min * (v->min < vars.a->arr[v->i])
				+ vars.a->arr[v->i] * !(v->min < vars.a->arr[v->i]);
			v->min_index = v->min_index * (v->min < vars.a->arr[v->i])
				+ v->i * !(v->min < vars.a->arr[v->i]);
		}
		v->i++;
	}
}
