/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   envade_conquer.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: amajid <amajid@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/01/03 17:51:22 by amajid            #+#    #+#             */
/*   Updated: 2024/01/03 19:51:13 by amajid           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft/libft.h"
#include "push_swap.h"
#include <stdio.h>
#include <unistd.h>

int	sort_work(t_stack *a, t_stack *b, t_v2 *v)
{
	char	is_smaller;

	while (v->i < b->size)
	{
		v->cost = get_cost((t_vars[]){{a, b, v->i, &v->a_i,
				.is_a_down = &v->is_a_down, .is_b_down = &v->is_b_down}});
		if (v->cost == -1)
			return (-1);
		is_smaller = (v->min_cost < v->cost);
		v->min_cost = v->min_cost * is_smaller + v->cost * !is_smaller;
		v->b_index = v->b_index * is_smaller + v->i * !is_smaller;
		v->a_index = v->a_index * is_smaller + v->a_i * !is_smaller;
		v->is_a_d = v->is_a_d * is_smaller + v->is_a_down * !is_smaller;
		v->is_b_d = v->is_b_d * is_smaller + v->is_b_down * !is_smaller;
		v->i++;
	}
	return (0);
}

void	sort(t_stack *a, t_stack *b)
{
	t_v2	v;

	v.i = 0;
	v.min_cost = get_cost((t_vars[]){{a, b, 0,
			&v.a_i, .is_a_down = &v.is_a_down, .is_b_down = &v.is_b_down}});
	if (v.min_cost == -1)
		return ;
	v.b_index = 0;
	v.a_index = v.a_i;
	v.is_a_d = v.is_a_down;
	v.is_b_d = v.is_b_down;
	if (sort_work(a, b, &v))
		return ;
	push_swap((t_v3){.a = a, .b = b, .a_index = &v.a_index,
		.b_index = &v.b_index,}, v.is_a_d, v.is_b_d);
}

int	work_0(t_v1 *v, t_stack *a, t_stack *b)
{
	if (a->size <= 2 && a->arr[0] > a->arr[1])
	{
		ra(a);
		return (-1);
	}
	else if (a->size <= 2)
		return (-1);
	v->min = a->arr[0];
	while (v->i < (a->size))
	{
		v->min = get_min(v->min, a->arr[v->i]);
		v->i++;
	}
	push_but_3(a, b);
	sort_a(a);
	while (b->size)
	{
		sort(a, b);
	}
	return (0);
}

void	envade_conqer(t_stack *a, t_stack *b)
{
	t_v1	v;
	char	is_up;

	v = (t_v1){0};
	if (work_0(&v, a, b) == -1)
		return ;
	v.i = 0;
	while (a->arr[v.i] != v.min)
		v.i++;
	is_up = v.i <= (a->size / 2);
	while (v.i)
	{
		if (is_up)
		{
			ra(a);
			v.i--;
		}
		else
		{
			rra(a);
			v.i++;
			if (v.i >= a->size)
				v.i = 0;
		}
	}
}
