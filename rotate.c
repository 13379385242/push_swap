/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rotate.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: amajid <amajid@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/01/03 18:56:22 by amajid            #+#    #+#             */
/*   Updated: 2024/01/03 18:57:08 by amajid           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void	rotate(t_stack *s)
{
	int	tmp;

	tmp = s->arr[0];
	ft_memmove(s->arr, s->arr + 1, s->size * sizeof(int));
	s->arr[s->size - 1] = tmp;
}

void	ra(t_stack *a)
{
	if (a->size >= 2)
	{
		ft_putendl_fd("ra", 1);
		rotate(a);
	}
}

void	rb(t_stack *b)
{
	if (b->size >= 2)
	{
		ft_putendl_fd("rb", 1);
		rotate(b);
	}
}

void	rr(t_stack *a, t_stack *b)
{
	if (a->size >= 2 && b->size >= 2)
	{
		ft_putendl_fd("rr", 1);
		rotate(a);
		rotate(b);
	}
}
