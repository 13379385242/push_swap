/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_stack.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: amajid <amajid@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/12/27 18:47:34 by amajid            #+#    #+#             */
/*   Updated: 2024/01/03 19:04:45 by amajid           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"
#include "libft/libft.h"
#include "push_swap.h"

void	print_stack(t_stack *stack)
{
	unsigned int		i;

	i = 0;
	while (i < stack->size)
	{
		ft_putnbr_fd(stack->arr[i], 1);
		ft_putendl_fd("", 1);
		i++;
	}
}
