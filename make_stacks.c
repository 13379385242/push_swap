/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   make_stacks.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: amajid <amajid@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/12/27 18:47:27 by amajid            #+#    #+#             */
/*   Updated: 2024/01/03 19:51:35 by amajid           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft/libft.h"
#include <limits.h>
#include <stdlib.h>
#include <stdio.h>
#include "push_swap.h"

void	realloc_int(int **result, long *size)
{
	int	*tmp;
	int	mul;

	mul = 3;
	tmp = malloc(sizeof(int) * (*size));
	if (!tmp)
	{
		free(*result);
		exit(0);
	}
	ft_memcpy(tmp, *result, sizeof(int) * (*size));
	free(*result);
	*result = malloc(sizeof(int) * ((*size) * mul));
	if (!(*result))
		exit(0);
	ft_memcpy(*result, tmp, sizeof(int) * (*size));
	free(tmp);
	(*size) *= mul;
}

void	make_stacks_work(t_vars2 *v, int count, char **elements)
{
	while (v->i < count)
	{
		v->j = 0;
		v->ss = ft_split(elements[v->i], ' ');
		if (!v->ss[0])
		{
			ft_putstr_fd("Error\n", 1);
			free(v->stack);
			exit(0);
		}
		while (v->ss[v->j])
		{
			if (v->i == 0 && v->j == 0)
			{
				v->j++;
				continue ;
			}
			v->stack[v->index] = ft_atoi(v->ss[v->j]);
			v->j++;
			v->index++;
			if (v->index >= v->s_size)
				realloc_int(&v->stack, &v->s_size);
		}
		v->i++;
	}
}

t_stack	make_staks(int count, char **elements)
{
	t_vars2	v;

	v.s_size = 100;
	v.stack = malloc(sizeof(int) * v.s_size);
	if (!v.stack)
	{
		ft_putstr_fd("Error\n", 1);
		exit(0);
	}
	check_for_eroors_0(count, elements, v.stack);
	v.i = 0;
	v.ss = ft_split(elements[0], ' ');
	if (!v.ss[0])
	{
		ft_putstr_fd("Error\n", 1);
		free(v.stack);
		exit(0);
	}
	v.stack[0] = ft_atoi(v.ss[0]);
	v.index = 1;
	make_stacks_work(&v, count, elements);
	v.result = (t_stack){.arr = v.stack, .size = v.index};
	return (v.result);
}
