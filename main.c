/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: amajid <amajid@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/01/03 19:02:28 by amajid            #+#    #+#             */
/*   Updated: 2024/01/03 19:02:42 by amajid           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft/libft.h"
#include "push_swap.h"
#include <stdio.h>
#include <stdlib.h>

int	main(int argnum, char **args)
{
	t_stack	stack_a;
	t_stack	stack_b;

	if (argnum == 1)
		exit(0);
	stack_a = make_staks(argnum - 1, args + 1);
	check_for_duplicates(stack_a);
	stack_b = create_stack_b(stack_a.size);
	envade_conqer(&stack_a, &stack_b);
	free(stack_a.arr);
	free(stack_b.arr);
	return (0);
}
