/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   reverse_rotate.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: amajid <amajid@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/01/03 18:59:20 by amajid            #+#    #+#             */
/*   Updated: 2024/01/03 19:04:24 by amajid           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"
#include "libft/libft.h"
#include <stdio.h>

void	reverse_rotate(t_stack *s)
{
	int	tmp;

	tmp = s->arr[s->size - 1];
	ft_memmove(s->arr + 1, s->arr, (s->size - 1) * sizeof(int));
	s->arr[0] = tmp;
}

void	rra(t_stack *a)
{
	if (a->size >= 2)
	{
		ft_putendl_fd("rra", 1);
		reverse_rotate(a);
	}
}

void	rrb(t_stack *b)
{
	if (b->size >= 2)
	{
		ft_putendl_fd("rrb", 1);
		reverse_rotate(b);
	}
}

void	rrr(t_stack *a, t_stack *b)
{
	if (a->size >= 2 && b->size >= 2)
	{
		ft_putendl_fd("rrr", 1);
		reverse_rotate(a);
		reverse_rotate(b);
	}
}
