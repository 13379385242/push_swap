/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: amajid <amajid@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/01/03 18:52:07 by amajid            #+#    #+#             */
/*   Updated: 2024/01/03 19:03:59 by amajid           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void	push(t_stack *to, t_stack *from)
{
	ft_memmove(to->arr + 1, to->arr, to->size * sizeof(int));
	to->arr[0] = from->arr[0];
	ft_memmove(from->arr, from->arr + 1,
		(from->size - 1) * sizeof(int));
	to->size++;
	from->size--;
}

void	pa(t_stack *a, t_stack *b)
{
	if (b->size >= 1)
	{
		ft_putendl_fd("pa", 1);
		push(a, b);
	}
}

void	pb(t_stack *a, t_stack *b)
{
	if (a->size >= 1)
	{
		ft_putendl_fd("pb", 1);
		push(b, a);
	}
}
