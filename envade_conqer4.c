/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   envade_conqer4.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: amajid <amajid@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/01/03 18:10:28 by amajid            #+#    #+#             */
/*   Updated: 2024/01/03 18:11:12 by amajid           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

long	gcu_a_part1(t_gcu_a *v, t_vars vars)
{
	v->is_neg = v->min_index > (vars.a->size / 2);
	if (v->is_neg)
		v->cost_to_up = vars.a->size - v->min_index;
	else
		v->cost_to_up = v->min_index;
	if (v->is_neg && !(*vars.is_b_down))
	{
		v->new_cost_up = v->min_index - vars.index;
		v->new_cost_up *= -1 * (v->new_cost_up < 0) + !(v->new_cost_up < 0);
	}
	if (!v->is_neg && (*vars.is_b_down))
	{
		v->new_cost_up = (vars.a->size - v->min_index)
			- (vars.b->size - vars.index);
		v->new_cost_up *= -1 * (v->new_cost_up < 0) + !(v->new_cost_up < 0);
	}
	if (v->is_neg != (*vars.is_b_down) && v->new_cost_up < (v->cost_to_up))
	{
		*vars.res_index = v->min_index;
		*vars.is_a_down = *vars.is_b_down;
		if (*vars.b_cost < v->new_cost_up)
			*vars.b_cost = v->new_cost_up;
		return (0);
	}
	return (1);
}

long	get_cost_up_a(t_vars vars)
{
	t_gcu_a	v;

	v = (t_gcu_a){.is_b_neg = vars.index > (vars.b->size / 2),
		.target = vars.b->arr[vars.index], .i = 0, .is_first = 1,
		.min = vars.a->arr[0], .min_index = -1};
	gcu_a_part0(&v, vars);
	if (v.min_index == -1)
	{
		push_swap_max(vars.a, vars.b, vars.index);
		return (-1);
	}
	if (!gcu_a_part1(&v, vars))
		return (0);
	*vars.res_index = v.min_index;
	*vars.is_a_down = v.is_neg;
	if (v.is_neg == *vars.is_b_down)
	{
		if (*vars.b_cost < v.cost_to_up)
			*vars.b_cost = v.cost_to_up;
		return (0);
	}
	*vars.b_cost += v.cost_to_up;
	return (0);
}

long	get_cost(t_vars *v)
{
	long	cost;
	long	ret_a;

	cost = get_cost_up_b(v->a, v->b, v->index, v->is_b_down);
	v->b_cost = &cost;
	ret_a = get_cost_up_a(*v);
	if (ret_a == -1)
		return (-1);
	return (cost);
}

void	sort_a(t_stack *a)
{
	int	*ar;

	ar = a->arr;
	if (ar[0] < ar[1] && ar[1] > ar[2] && ar[0] < ar[2])
	{
		rra(a);
		sa(a);
	}
	else if (ar[0] > ar[1] && ar[0] > ar[2] && ar[1] < ar[2])
		ra(a);
	else if (ar[0] > ar[1] && ar[1] > ar[2])
	{
		sa(a);
		rra(a);
	}
	else if (ar[0] < ar[1] && ar[0] > ar[2])
		rra(a);
	else if (ar[0] > ar[1] && ar[1] < ar[2] && ar[0] < ar[2])
		sa(a);
}
