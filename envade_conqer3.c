/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   envade_conqer3.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: amajid <amajid@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/01/03 18:02:56 by amajid            #+#    #+#             */
/*   Updated: 2024/01/03 18:16:35 by amajid           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void	ps_0(t_v3 v)
{
	v.comulative_reduce = (*v.a_index) * ((*v.a_index) < (*v.b_index))
		+ (*v.b_index) * !((*v.a_index) < (*v.b_index));
	while (v.comulative_reduce)
	{
		rr(v.a, v.b);
		v.comulative_reduce--;
		(*v.a_index)--;
		(*v.b_index)--;
	}
}

void	ps_1(t_v3 v)
{
	char	is_a;

	is_a = ((v.a->size - (*v.a_index)) < (v.b->size - (*v.b_index)));
	v.comulative_reduce = (v.a->size - (*v.a_index)) * is_a
		+ (v.b->size - (*v.b_index)) * !(is_a);
	while (v.comulative_reduce)
	{
		rrr(v.a, v.b);
		v.comulative_reduce--;
		(*v.a_index)++;
		(*v.b_index)++;
		if ((*v.a_index) >= v.a->size)
			(*v.a_index) = 0;
		if ((*v.b_index) >= v.b->size)
			(*v.b_index) = 0;
	}
}

void	ps_2(t_v3 v)
{
	while ((*v.a_index))
	{
		if (v.a_is_up)
		{
			ra(v.a);
			(*v.a_index)--;
		}
		else
		{
			rra(v.a);
			(*v.a_index)++;
			if ((*v.a_index) >= v.a->size)
				(*v.a_index) = 0;
		}
	}
}

void	ps_3(t_v3 v)
{
	while ((*v.b_index))
	{
		if (v.b_is_up)
		{
			rb(v.b);
			(*v.b_index)--;
		}
		else
		{
			rrb(v.b);
			(*v.b_index)++;
			if ((*v.b_index) >= v.b->size)
				(*v.b_index) = 0;
		}
	}
}

void	push_swap(t_v3 v_p, char is_a_d, char is_b_d)
{
	char	a_is_up;
	char	b_is_up;
	t_v3	v;

	a_is_up = !is_a_d;
	b_is_up = !is_b_d;
	v = (t_v3){a_is_up, b_is_up, 0, v_p.a, v_p.b, v_p.a_index, v_p.b_index};
	if (a_is_up == b_is_up && v_p.a->size >= 2
		&& v_p.b->size >= 2)
	{
		if (a_is_up)
		{
			ps_0(v);
		}
		else if (v_p.a->size >= 2 && v_p.b->size >= 2)
		{
			ps_1(v);
		}
	}
	ps_2(v);
	ps_3(v);
	pa(v_p.a, v_p.b);
}
