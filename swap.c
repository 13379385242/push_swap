/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   swap.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: amajid <amajid@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/01/03 18:49:33 by amajid            #+#    #+#             */
/*   Updated: 2024/01/03 18:50:10 by amajid           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void	swap(t_stack *s)
{
	int	tmp;

	tmp = s->arr[0];
	s->arr[0] = s->arr[1];
	s->arr[1] = tmp;
}

void	sa(t_stack *a)
{
	if (a->size >= 2)
	{
		ft_putendl_fd("sa", 1);
		swap(a);
	}
}

void	sb(t_stack *b)
{
	if (b->size >= 2)
	{
		ft_putendl_fd("sb", 1);
		swap(b);
	}
}

void	ss(t_stack *a, t_stack *b)
{
	if (a->size >= 2 && b->size >= 2)
	{
		ft_putendl_fd("ss", 1);
		swap(a);
		swap(b);
	}
}
