SRCS = make_stacks.c make_stacks2.c print_stack.c reverse_rotate.c envade_conquer.c check_for_duplicates.c envade_conqer2.c envade_conqer3.c envade_conqer4.c swap.c push.c rotate.c
OBJS = $(SRCS:.c=.o)
BONUS_SRCS = 
BONUS_OBJS = $(BONUS_SRCS:.c=.o)
GNL_SRCS = 
GNL_OBJS = $(GNL_SRCS:.c=.o)
CC = cc
AR = ar
C_FLAGS = -Wall -Wextra -Werror -Ofast
LD_FLAGS := -L./libft -lft 
NAME = push_swap
LD_FLAGS_OBJ = 
.PHONY: clean

all: $(NAME)


clean:
	cd libft && make fclean
	rm -rf $(OBJS)  $(BONUS_OBJS) $(GNL_OBJS) debug bonus b_debug main.o

fclean: clean
	rm -rf $(NAME)

re: fclean all


%.o: %.c push_swap.h
	$(CC) $(C_FLAGS) -c -o $@ $< $(LD_FLAGS_OBJ)

$(NAME):  libft/libft.a main.o $(OBJS) $(GNL_OBJS)
	$(CC) $(C_FLAGS) -o  $@ main.o $(OBJS) $(GNL_OBJS) $(LD_FLAGS) 

libft/libft.a:
	cd libft && make fclean bonus


bonus: libft/libft.a $(OBJS) $(BONUS_OBJS) $(GNL_OBJS)
	$(CC) $(C_FLAGS) -o  $@ $(OBJS)  $(BONUS_OBJS) $(GNL_OBJS) $(LD_FLAGS) 

debug: C_FLAGS+= -g
debug: LD_FLAGS+= -fsanitize=address
debug: LD_FLAGS_OBJ+= -fsanitize=address
debug:  libft/libft.a main.o  $(OBJS) $(GNL_OBJS)
	$(CC) $(C_FLAGS) -o $@ main.o $(OBJS) $(GNL_OBJS) $(LD_FLAGS)


b_debug: C_FLAGS+= -g
b_debug: LD_FLAGS+= -fsanitize=address
b_debug: LD_FLAGS_OBJ+= -fsanitize=address
b_debug: libft/libft.a $(OBJS) $(BONUS_OBJS) $(GNL_OBJS)
	$(CC) $(C_FLAGS) -o  $@ $(OBJS)  $(BONUS_OBJS) $(GNL_OBJS) $(LD_FLAGS)

