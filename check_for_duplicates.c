/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_for_duplicates.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: amajid <amajid@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/01/03 18:48:18 by amajid            #+#    #+#             */
/*   Updated: 2024/01/03 18:48:38 by amajid           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"
#include <stdlib.h>

void	check_for_duplicates(t_stack a)
{
	long	i;
	long	j;

	i = 0;
	while (i < a.size)
	{
		j = 0;
		while (j < a.size && j != i)
		{
			if (a.arr[i] == a.arr[j])
			{
				ft_putstr_fd("Error\n", 1);
				free(a.arr);
				exit(0);
			}
			j++;
		}
		i++;
	}
}
