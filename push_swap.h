/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_swap.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: amajid <amajid@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/01/03 18:15:36 by amajid            #+#    #+#             */
/*   Updated: 2024/01/03 19:48:38 by amajid           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#pragma once
#include "libft/libft.h"

typedef struct stack
{
	int				*arr;
	unsigned int	size;
}	t_stack;

typedef struct gcu_a
{
	long	cost_to_up;
	long	i;
	int		target;
	int		min;
	long	min_index;
	char	is_neg;
	char	is_b_neg;
	char	is_first;
	long	new_cost_up;
}	t_gcu_a;

typedef struct vars
{
	t_stack		*a;
	t_stack		*b;
	long long	index;
	long		*res_index;
	long		*b_cost;
	char		*is_a_down;
	char		*is_b_down;
}	t_vars;

typedef struct v3
{
	char	a_is_up;
	char	b_is_up;
	long	comulative_reduce;
	t_stack	*a;
	t_stack	*b;
	long	*a_index;
	long	*b_index;
}	t_v3;

typedef struct v2
{
	long	i;
	long	a_i;
	long	b_index;
	long	a_index;
	long	cost;
	long	min_cost;
	char	is_a_down;
	char	is_b_down;
	char	is_a_d;
	char	is_b_d;
}	t_v2;

typedef struct v1
{
	char			is_sorted;
	unsigned int	i;
	int				min;
}	t_v1;

typedef struct err_v
{
	int				i;
	char			is_mult_sign;
	long long		num;
	unsigned int	j;
	char			**ss;
	char			is_n_fount;
}	t_err_v;

typedef struct vars2
{
	int				*stack;
	int				i;
	unsigned int	j;
	char			**ss;
	unsigned int	index;
	long			s_size;
	t_stack			result;
}	t_vars2;

t_stack	make_staks(int count, char **elements);
void	print_stack(t_stack *stack);
t_stack	create_stack_b(int count);
void	sa(t_stack *a);
void	sb(t_stack *b);
void	ss(t_stack *a, t_stack *b);
void	pa(t_stack *a, t_stack *b);
void	pb(t_stack *a, t_stack *b);
void	ra(t_stack *a);
void	rb(t_stack *b);
void	rr(t_stack *a, t_stack *b);
void	rra(t_stack *a);
void	rrb(t_stack *b);
void	rrr(t_stack *a, t_stack *b);
void	envade_conqer(t_stack *a, t_stack *b);
void	check_for_duplicates(t_stack a);
int		get_min(int x, int y);
long	get_cost_up_b(t_stack *a, t_stack *b, long long index, char *is_b_up);
void	push_swap_max(t_stack *a, t_stack *b, long index);
void	gcu_a_part0(t_gcu_a *v, t_vars vars);
void	push_but_3(t_stack *a, t_stack *b);
void	push_swap(t_v3 v_p, char is_a_d, char is_b_d);
void	sort_a(t_stack *a);
long	get_cost(t_vars *v);
char	is_err_work(t_err_v *v);
char	is_err(char *s);
char	check_for_eroors_0(int count, char **elements, int *a);
t_stack	create_stack_b(int count);